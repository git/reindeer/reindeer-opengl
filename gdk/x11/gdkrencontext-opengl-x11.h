/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GDK_REN_CONTEXT_OPENGL_X11_H
#define GDK_REN_CONTEXT_OPENGL_X11_H

#include <gdk/gdkrencontext.h>

#include <glib-object.h>
#include <GL/glx.h>

G_BEGIN_DECLS

#define GDK_REN_TYPE_CONTEXT_OPENGL_X11\
	(gdk_ren_context_opengl_x11_get_type ())
#define GDK_REN_CONTEXT_OPENGL_X11(obj)\
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), GDK_REN_TYPE_CONTEXT_OPENGL_X11, GdkRenContextOpenglX11))
#define GDK_REN_CONTEXT_OPENGL_X11_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_CAST ((klass), GDK_REN_TYPE_CONTEXT_OPENGL_X11, GdkRenContextOpenglX11Class))
#define GDK_REN_IS_CONTEXT_OPENGL_X11(obj)\
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GDK_REN_TYPE_CONTEXT_OPENGL_X11))
#define GDK_REN_IS_CONTEXT_OPENGL_X11_CLASS(klass)\
	(G_TYPE_CHECK_CLASS_TYPE ((klass), GDK_REN_TYPE_CONTEXT_OPENGL_X11))
#define GDK_REN_CONTEXT_OPENGL_X11_GET_CLASS(obj)\
	(G_TYPE_INSTANCE_GET_CLASS ((obj), GDK_REN_TYPE_CONTEXT_OPENGL_X11, GdkRenContextOpenglX11Class))

typedef struct _GdkRenContextOpenglX11 GdkRenContextOpenglX11;
typedef struct _GdkRenContextOpenglX11Class GdkRenContextOpenglX11Class;

typedef struct _GdkRenContextOpenglX11SwapControl GdkRenContextOpenglX11SwapControl;
struct _GdkRenContextOpenglX11SwapControl
{
	gint (* get_swap_interval) (GdkRenContextOpenglX11 *context);
	gboolean (* set_swap_interval) (GdkRenContextOpenglX11 *context, gint interval);
	union
	{
		void *dummy;
		#if GLX_MESA_swap_control
		struct _GdkRenContextOpenglX11SwapControlMesa
		{
			PFNGLXGETSWAPINTERVALMESAPROC glXGetSwapIntervalMESA;
			PFNGLXSWAPINTERVALMESAPROC glXSwapIntervalMESA;
		} mesa;
		#endif
	};
};

struct _GdkRenContextOpenglX11
{
	GdkRenContext parent;

	GLXContext glxcontext;
	GdkRenContextOpenglX11SwapControl swap_control;
};

struct _GdkRenContextOpenglX11Class
{
	GdkRenContextClass parent;
};

GType
gdk_ren_context_opengl_x11_get_type (void);

G_END_DECLS

#endif
