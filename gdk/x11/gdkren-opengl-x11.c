/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gdkren-opengl-x11.h"

#include <GL/glx.h>
#include <gdk/gdkx.h>
#include <string.h>

const gchar* gdk_ren__opengl_x11__compat_version = "0.2";

gboolean
gdk_ren_opengl_x11_glx_is_supported (GdkDisplay *display)
{
	int dummya = 0, dummyb = 0;
	Bool res;
	res = glXQueryExtension (GDK_DISPLAY_XDISPLAY (display), &dummya, &dummyb);
	if (res == False)
		return FALSE;
	res = glXQueryVersion (GDK_DISPLAY_XDISPLAY (display), &dummya, &dummyb);
	return (res != False);
}

gboolean
gdk_ren_opengl_x11_glx_query_version (GdkDisplay *display, gint *major, gint *minor)
{
	Bool res;
	int tmp_major, tmp_minor;
	res = glXQueryVersion (GDK_DISPLAY_XDISPLAY (display), &tmp_major, &tmp_minor);
	*major = tmp_major;
	if (tmp_major == 1 && g_getenv ("GDK_REN__OPENGL__FORCE_GLX_1_2") != NULL)
		*minor = MIN (tmp_minor, 2);
	else
		*minor = tmp_minor;
	return (res != False);
}

gboolean
gdk_ren_opengl_x11_glx_has_extension (GdkScreen *screen, gchar *query)
{
	#ifdef GLX_VERSION_1_1
	gint major, minor;
	if (!gdk_ren_opengl_x11_glx_query_version (gdk_screen_get_display (screen), &major, &minor))
		return FALSE;
	if (major == 1 && minor >= 1)
	{
		const char *glx_extensions = glXQueryExtensionsString (
			GDK_SCREEN_XDISPLAY (screen), GDK_SCREEN_XNUMBER (screen));
		const size_t len = strlen(query);
		const char *ptr = strstr (glx_extensions, query);
		return ((ptr != NULL) && ((ptr[len] == ' ') || (ptr[len] == '\0')));
	}
	#endif
	return FALSE;
}
