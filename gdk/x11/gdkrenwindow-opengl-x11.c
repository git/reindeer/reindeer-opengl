/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gdkrenwindow-opengl-x11.h"
#include "gdkrenconfig-opengl-x11.h"

#include <gdk/gdk.h>
#include <gdk/gdkx.h>

static void
gdk_ren_window_opengl_x11_swap_buffers (GdkRenDrawable *drawable);

G_DEFINE_DYNAMIC_TYPE (GdkRenWindowOpenglX11, gdk_ren_window_opengl_x11, GDK_REN_TYPE_WINDOW)
static void
gdk_ren_window_opengl_x11_finalize (GObject *gobject);

void
gdk_ren_window_opengl_x11_type_register (GTypeModule *type_module)
{
	gdk_ren_window_opengl_x11_register_type (type_module);
}

static void
gdk_ren_window_opengl_x11_init (GdkRenWindowOpenglX11 *self)
{
}

static void
gdk_ren_window_opengl_x11_finalize (GObject *gobject)
{
	GdkRenWindowOpenglX11 *window = (GdkRenWindowOpenglX11 *) gobject;
	#ifdef GLX_VERSION_1_3
	GdkRenWindow* renwindow = (GdkRenWindow *) gobject;
	GdkRenConfigOpenglX11 *config = (GdkRenConfigOpenglX11 *) renwindow->config;
	if (config->uses_fbconfig)
		glXDestroyWindow (window->xdisplay, window->glxwindow);
	#endif
	G_OBJECT_CLASS (gdk_ren_window_opengl_x11_parent_class)->finalize (gobject);
}

static void
gdk_ren_window_opengl_x11_class_init (GdkRenWindowOpenglX11Class *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	gobject_class->finalize = gdk_ren_window_opengl_x11_finalize;
	GdkRenDrawableClass *gdk_ren_drawable_class = GDK_REN_DRAWABLE_CLASS (klass);
	gdk_ren_drawable_class->swap_buffers = gdk_ren_window_opengl_x11_swap_buffers;
}

static void
gdk_ren_window_opengl_x11_class_finalize (GdkRenWindowOpenglX11Class *klass)
{
}

GdkRenWindow*
gdk_ren_window_opengl_x11_make (GdkRenConfig *renconfig, GdkWindow *gdkwindow,
	GError **error)
{
	g_return_val_if_fail (GDK_REN_IS_CONFIG_OPENGL_X11 (renconfig), NULL);
	g_return_val_if_fail (GDK_IS_WINDOW (gdkwindow), NULL);

	GdkRenConfigOpenglX11 *config = GDK_REN_CONFIG_OPENGL_X11 (renconfig);
	GdkDisplay *display = gdk_screen_get_display (renconfig->screen);
	Display *xdisplay = GDK_DISPLAY_XDISPLAY(display);
	if (xdisplay != GDK_DRAWABLE_XDISPLAY (GDK_DRAWABLE (gdkwindow)))
		return NULL;

	Window xwindow = GDK_WINDOW_XWINDOW (gdkwindow);
	GLXWindow glxwindow;
	#ifdef GLX_VERSION_1_3
	if (config->uses_fbconfig)
		glxwindow = glXCreateWindow (xdisplay, config->fbconfig, xwindow, NULL);
	else
	#endif
		glxwindow = xwindow;

	if (glxwindow == None)
	{
		g_set_error_literal (error, GDK_REN_ERROR, GDK_REN_ERROR_FAILED,
			"Failed to create GLXWindow");
		return NULL;
	}

	GdkRenWindowOpenglX11 *window =
		g_object_new (GDK_REN_TYPE_WINDOW_OPENGL_X11, NULL);
	window->glxwindow = glxwindow;
	window->xwindow = xwindow;
	window->xdisplay = xdisplay;
	GdkRenWindow *renwindow = GDK_REN_WINDOW (window);
	renwindow->config = g_object_ref (renconfig);
	renwindow->window = g_object_ref (gdkwindow);

	return renwindow;
}

static void
gdk_ren_window_opengl_x11_swap_buffers (GdkRenDrawable *drawable)
{
	GdkRenWindowOpenglX11 *window = GDK_REN_WINDOW_OPENGL_X11 (drawable);
	glXSwapBuffers (window->xdisplay, window->glxwindow);
}
