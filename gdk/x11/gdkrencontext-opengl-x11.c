/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gdkrencontext-opengl-x11.h"
#include "gdkrenconfig-opengl-x11.h"
#include "gdkrenwindow-opengl-x11.h"
#include "gdkren-opengl-x11.h"

#include <gdk/gdk.h>
#include <gdk/gdkx.h>
#include <gdk/gdkren.h>
#include <ren/ren.h>

static void
gdk_ren_context_opengl_x11_set_draw (GdkRenContext *context, GdkRenDrawable *draw);
static void
gdk_ren_context_opengl_x11_set_read (GdkRenContext *context, GdkRenDrawable *read);
static RenReindeer*
gdk_ren_context_opengl_x11_ren_begin (GdkRenContext *context);
static void
gdk_ren_context_opengl_x11_ren_end (GdkRenContext *context);

static gint
gdk_ren_context_opengl_x11_get_swap_interval (GdkRenContext *rencontext);
static gboolean
gdk_ren_context_opengl_x11_set_swap_interval (GdkRenContext *rencontext, gint interval);

static const GdkRenContextOpenglX11SwapControl*
gdk_ren_context_opengl_x11_get_swap_control (GdkScreen *screen);

#if GLX_MESA_swap_control
static gint
gdk_ren_context_opengl_x11_get_swap_interval_mesa (GdkRenContextOpenglX11 *context);
static gboolean
gdk_ren_context_opengl_x11_set_swap_interval_mesa (GdkRenContextOpenglX11 *context, gint interval);
#endif

G_DEFINE_DYNAMIC_TYPE (GdkRenContextOpenglX11, gdk_ren_context_opengl_x11, GDK_REN_TYPE_CONTEXT)
static void
gdk_ren_context_opengl_x11_finalize (GObject *gobject);

void
gdk_ren_context_opengl_x11_type_register (GTypeModule *type_module)
{
	gdk_ren_context_opengl_x11_register_type (type_module);
}

static void
gdk_ren_context_opengl_x11_init (GdkRenContextOpenglX11 *self)
{
}

static void
gdk_ren_context_opengl_x11_finalize (GObject *gobject)
{
	GdkRenContext* rencontext = (GdkRenContext *) gobject;
	GdkRenContextOpenglX11 *context = (GdkRenContextOpenglX11 *) gobject;
	Display *xdisplay = GDK_SCREEN_XDISPLAY (rencontext->config->screen);
	glXDestroyContext (xdisplay, context->glxcontext);
	G_OBJECT_CLASS (gdk_ren_context_opengl_x11_parent_class)->finalize (gobject);
}

static void
gdk_ren_context_opengl_x11_class_init (GdkRenContextOpenglX11Class *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	gobject_class->finalize = gdk_ren_context_opengl_x11_finalize;
	GdkRenContextClass *rencontext_class = GDK_REN_CONTEXT_CLASS (klass);
	rencontext_class->set_draw = gdk_ren_context_opengl_x11_set_draw;
	rencontext_class->set_read = gdk_ren_context_opengl_x11_set_read;
	rencontext_class->ren_begin = gdk_ren_context_opengl_x11_ren_begin;
	rencontext_class->ren_end = gdk_ren_context_opengl_x11_ren_end;
	rencontext_class->get_swap_interval = gdk_ren_context_opengl_x11_get_swap_interval;
	rencontext_class->set_swap_interval = gdk_ren_context_opengl_x11_set_swap_interval;
}

static void
gdk_ren_context_opengl_x11_class_finalize (GdkRenContextOpenglX11Class *klass)
{
}

GdkRenContext*
gdk_ren_context_opengl_x11_make (GdkRenConfig *renconfig,
	GdkRenContext *share, GdkRenRenderType render_type,
	GdkRenDrawable *draw, GdkRenDrawable *read, GError **error)
{
	g_return_val_if_fail (draw == NULL || GDK_REN_IS_WINDOW_OPENGL_X11 (draw), NULL);
	g_return_val_if_fail (read == NULL || GDK_REN_IS_WINDOW_OPENGL_X11 (read), NULL);
	g_return_val_if_fail (GDK_REN_IS_CONFIG_OPENGL_X11 (renconfig), NULL);
	g_return_val_if_fail (share == NULL || GDK_REN_IS_CONTEXT_OPENGL_X11 (share), NULL);

	GdkRenBackend *backend = gdk_ren_config_get_backend (renconfig);
	GdkRenConfigOpenglX11 *config = GDK_REN_CONFIG_OPENGL_X11 (renconfig);

	GdkScreen *screen = renconfig->screen;
	Display *xdisplay = GDK_SCREEN_XDISPLAY (screen);

	GLXContext glxshare = NULL;
	if (share != NULL)
		glxshare = GDK_REN_CONTEXT_OPENGL_X11 (share)->glxcontext;

	int glx_render_type;
	if (render_type & GDK_REN_RENDER_TYPE_RGBA)
		glx_render_type = GLX_RGBA_TYPE;
	else if (render_type & GDK_REN_RENDER_TYPE_INDEX)
		glx_render_type = GLX_COLOR_INDEX_TYPE;
	else
	{
		g_set_error_literal (error, GDK_REN_ERROR,
			GDK_REN_ERROR_RENDER_TYPE_UNSUPPORTED, NULL);
		goto FAIL;
	}

	GLXContext glxcontext;
	#ifdef GLX_VERSION_1_3
	if (config->uses_fbconfig)
	{
		glxcontext = glXCreateNewContext (xdisplay,
			config->fbconfig, glx_render_type, glxshare, True);
	}
	else
	#endif
	{
		glxcontext = glXCreateContext (xdisplay,
			&(config->visual), glxshare, True);
	}
	if (glxcontext == NULL)
	{
		g_set_error_literal (error, GDK_REN_ERROR, GDK_REN_ERROR_FAILED,
			"Failed to create GLXContext");
		goto FAIL;
	}

	RenBackend *renbackend = gdk_ren_backend_get_ren_backend (backend);
	RenError *rerror = NULL;
	RenReindeer *r = ren_reindeer_new (renbackend, &rerror);
	if (r == NULL)
	{
		glXDestroyContext (xdisplay, glxcontext);
		g_set_error_literal (error, GDK_REN_ERROR,
			GDK_REN_ERROR_FAILED, rerror->message);
		ren_error_free (rerror);
		goto FAIL;
	}

	GdkRenContextOpenglX11 *context =
		g_object_new (GDK_REN_TYPE_CONTEXT_OPENGL_X11, NULL);
	context->glxcontext = glxcontext;
	context->swap_control = *gdk_ren_context_opengl_x11_get_swap_control (screen);

	GdkRenContext *rencontext = GDK_REN_CONTEXT (context);
	rencontext->config = g_object_ref (renconfig);
	rencontext->share = (share != NULL) ? g_object_ref (share) : NULL;
	rencontext->render_type = render_type;
	rencontext->draw = (draw != NULL) ? g_object_ref (draw) : NULL;
	rencontext->read = (read != NULL) ? g_object_ref (read) : NULL;
	rencontext->r = r;

	return rencontext;

	FAIL:
	return NULL;
}

static void
gdk_ren_context_opengl_x11_set_draw (GdkRenContext *rencontext, GdkRenDrawable *draw)
{
	g_return_if_fail (GDK_REN_IS_CONTEXT_OPENGL_X11 (rencontext));
	g_return_if_fail (GDK_REN_IS_WINDOW_OPENGL_X11 (draw));
	if (rencontext->draw != NULL)
		g_object_unref (rencontext->draw);
	rencontext->draw = (draw != NULL) ? g_object_ref (draw) : NULL;
}
static void
gdk_ren_context_opengl_x11_set_read (GdkRenContext *rencontext, GdkRenDrawable *read)
{
	g_return_if_fail (GDK_REN_IS_CONTEXT_OPENGL_X11 (rencontext));
	g_return_if_fail (GDK_REN_IS_WINDOW_OPENGL_X11 (read));
	if (rencontext->read != NULL)
		g_object_unref (rencontext->read);
	rencontext->read = (read != NULL) ? g_object_ref (read) : NULL;
}

static RenReindeer*
gdk_ren_context_opengl_x11_ren_begin (GdkRenContext *rencontext)
{
	g_return_val_if_fail (GDK_REN_IS_CONTEXT_OPENGL_X11 (rencontext), NULL);
	GdkRenContextOpenglX11 *context = GDK_REN_CONTEXT_OPENGL_X11 (rencontext);

	GdkDisplay *display = gdk_screen_get_display (rencontext->config->screen);
	Display *xdisplay = gdk_x11_display_get_xdisplay (display);
	GdkRenConfigOpenglX11 *config = (GdkRenConfigOpenglX11 *) rencontext->config;

	#if GLX_VERSION_1_3
	if (config->uses_fbconfig)
	{
		GLXDrawable xdraw = GDK_REN_WINDOW_OPENGL_X11 (rencontext->draw)->glxwindow;
		GLXDrawable xread = GDK_REN_WINDOW_OPENGL_X11 (rencontext->read)->glxwindow;

		if (glXMakeContextCurrent (xdisplay, xdraw, xread, context->glxcontext) == False)
			return NULL;
	}
	else
	#endif
	{
		Window xwindow = GDK_WINDOW_XWINDOW (GDK_REN_WINDOW (rencontext->draw)->window);
		if (glXMakeCurrent (xdisplay, xwindow, context->glxcontext) == False)
			return NULL;
	}

	return rencontext->r;
}
static void
gdk_ren_context_opengl_x11_ren_end (GdkRenContext *rencontext)
{
	g_return_if_fail (GDK_REN_IS_CONTEXT_OPENGL_X11 (rencontext));

	GdkRenConfig *renconfig = rencontext->config;
	GdkRenConfigOpenglX11 *config = (GdkRenConfigOpenglX11 *) renconfig;
	GdkDisplay *display = gdk_screen_get_display (renconfig->screen);
	Display *xdisplay = gdk_x11_display_get_xdisplay (display);

	#if GLX_VERSION_1_3
	if (config->uses_fbconfig)
		glXMakeContextCurrent (xdisplay, None, None, NULL);
	else
	#endif
		glXMakeCurrent (xdisplay, None, NULL);
}

static gint
gdk_ren_context_opengl_x11_get_swap_interval (GdkRenContext *rencontext)
{
	g_return_val_if_fail (GDK_REN_IS_CONTEXT_OPENGL_X11 (rencontext), -1);
	/* TODO: Check if current context. */

	GdkRenContextOpenglX11 *context = GDK_REN_CONTEXT_OPENGL_X11 (rencontext);
	if (context->swap_control.get_swap_interval != NULL)
		return context->swap_control.get_swap_interval (context);
	else
		return -1;
}

static gboolean
gdk_ren_context_opengl_x11_set_swap_interval (GdkRenContext *rencontext, gint interval)
{
	g_return_val_if_fail (GDK_REN_IS_CONTEXT_OPENGL_X11 (rencontext), FALSE);
	/* TODO: Check if current context. */

	GdkRenContextOpenglX11 *context = GDK_REN_CONTEXT_OPENGL_X11 (rencontext);
	if (context->swap_control.set_swap_interval != NULL)
		return context->swap_control.set_swap_interval (context, interval);
	else
		return FALSE;
}

static const GdkRenContextOpenglX11SwapControl*
gdk_ren_context_opengl_x11_get_swap_control (GdkScreen *screen)
{
	static GHashTable *swap_controls = NULL;
	if (G_UNLIKELY (swap_controls == NULL))
	{
		swap_controls = g_hash_table_new_full (g_direct_hash, g_direct_equal, NULL, g_free);
	}
	GdkRenContextOpenglX11SwapControl* swap_control;
	swap_control = g_hash_table_lookup (swap_controls, screen);
	if (swap_control == NULL)
	{
		swap_control = g_new0 (GdkRenContextOpenglX11SwapControl, 1);
		#if GLX_MESA_swap_control
		if (gdk_ren_opengl_x11_glx_has_extension (screen, "GLX_MESA_swap_control"))
		{
			g_message ("GLX_MESA_swap_control is supported");
			swap_control->get_swap_interval = gdk_ren_context_opengl_x11_get_swap_interval_mesa;
			swap_control->set_swap_interval = gdk_ren_context_opengl_x11_set_swap_interval_mesa;
			swap_control->mesa.glXGetSwapIntervalMESA = (PFNGLXGETSWAPINTERVALMESAPROC)
				glXGetProcAddress ((const GLubyte *) "glXGetSwapIntervalMESA");
			swap_control->mesa.glXSwapIntervalMESA = (PFNGLXSWAPINTERVALMESAPROC)
				glXGetProcAddress ((const GLubyte *) "glXSwapIntervalMESA");
		}
		#endif
		g_hash_table_insert (swap_controls, screen, swap_control);
		/* FIXME: Also connect to GdkDisplay::closed to remove it from
		hash table again. Though, this will require putting the table
		outside the function (which should be done anyway since we must
		free it sometime). An idea is to put it in class init/fini.  */
	}
	return swap_control;
}

#if GLX_MESA_swap_control
static gint
gdk_ren_context_opengl_x11_get_swap_interval_mesa (GdkRenContextOpenglX11 *context)
{
	/* TODO: Check if current context. */

	return context->swap_control.mesa.glXGetSwapIntervalMESA ();
}

static gboolean
gdk_ren_context_opengl_x11_set_swap_interval_mesa (GdkRenContextOpenglX11 *context, gint interval)
{
	/* TODO: Check if current context. */

	int res = context->swap_control.mesa.glXSwapIntervalMESA (interval);
	return (res == 0);
}
#endif
