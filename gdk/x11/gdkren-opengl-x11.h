/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GDK_REN_OPENGL_X11_H
#define GDK_REN_OPENGL_X11_H

#include <glib.h>
#include <gdk/gdk.h>

gboolean
gdk_ren_opengl_x11_glx_is_supported (GdkDisplay *display);

gboolean
gdk_ren_opengl_x11_glx_query_version (GdkDisplay *display, gint *major, gint *minor);

gboolean
gdk_ren_opengl_x11_glx_has_extension (GdkScreen *screen, gchar *query);

#endif /* GDK_REN_OPENGL_X11_H */
