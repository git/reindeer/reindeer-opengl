/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gdkrenconfig-opengl-x11.h"
#include "gdkren-opengl-x11.h"

#include <glib.h>
#include <gdk/gdk.h>
#include <gdk/gdkx.h>

#ifdef GLX_VERSION_1_3
static gboolean
fbconfig_to_attr (Display *xdisplay, GLXFBConfig fbconfig, GdkRenConfigAttr *attr);
#endif
static gboolean
visual_to_attr (Display *xdisplay, XVisualInfo *visual, GdkRenConfigAttr *attr);
#if 0
static GArray*
attr_to_glxattr (const GdkRenConfigAttr *attr);
#endif

G_DEFINE_DYNAMIC_TYPE (GdkRenConfigOpenglX11, gdk_ren_config_opengl_x11, GDK_REN_TYPE_CONFIG)

void
gdk_ren_config_opengl_x11_type_register (GTypeModule *type_module)
{
	gdk_ren_config_opengl_x11_register_type (type_module);
}

static void
gdk_ren_config_opengl_x11_init (GdkRenConfigOpenglX11 *self)
{
}

static void
gdk_ren_config_opengl_x11_class_init (GdkRenConfigOpenglX11Class *klass)
{
}

static void
gdk_ren_config_opengl_x11_class_finalize (GdkRenConfigOpenglX11Class *klass)
{
}

GdkRenConfig*
gdk_ren_config_opengl_x11_find (GdkRenBackend *backend, GdkScreen *screen,
	const GdkRenConfigAttr *attr, GdkRenConfigAttrCompareFunc compare,
	GError **error)
{
	if (screen == NULL)
		screen = gdk_screen_get_default ();
	else
		g_return_val_if_fail (GDK_IS_SCREEN (screen), NULL);
	if (attr == NULL)
		attr = &GDK_REN_CONFIG_ATTR_DEFAULT;
	if (compare == NULL)
		compare = gdk_ren_config_attr_compare_default;

	if (!gdk_ren_opengl_x11_glx_is_supported (gdk_screen_get_display (screen)))
	{
		g_set_error_literal (error, GDK_REN_ERROR, GDK_REN_ERROR_FAILED,
			"The OpenGL extension to the X Window System is not present");
		return NULL;
	}

	Display *xdisplay = GDK_SCREEN_XDISPLAY (screen);
	int xscreen = gdk_x11_screen_get_screen_number (screen);

	#ifdef GLX_VERSION_1_3
	gint major, minor;
	if (!gdk_ren_opengl_x11_glx_query_version (gdk_screen_get_display (screen), &major, &minor))
	{
		g_set_error_literal (error, GDK_REN_ERROR, GDK_REN_ERROR_FAILED,
			"Could not query the GLX version");
		return NULL;
	}
	if (major == 1 && minor >= 3)
	{
		int num_configs;
		GLXFBConfig *fbconfigs = glXGetFBConfigs (xdisplay, xscreen, &num_configs);
		if (num_configs == 0)
		{
			g_set_error_literal (error, GDK_REN_ERROR, GDK_REN_ERROR_FAILED,
				"There are no GLXFBConfigs available");
			return NULL;
		}

		int i;
		GLXFBConfig best_fbconfig = 0;
		GdkRenConfigAttr *best = NULL;
		GdkRenConfigAttr *current = g_new (GdkRenConfigAttr, 1);
		for (i = 0; i < num_configs; ++i)
		{
			GLXFBConfig fbconfig = fbconfigs[i];
			if (!fbconfig_to_attr (xdisplay, fbconfig, current))
				continue;
			if (gdk_ren_config_attr_match (current, attr))
			{
				if (best == NULL || compare (best, current))
				{
					GdkRenConfigAttr *prevbest = best;
					best = current;
					best_fbconfig = fbconfig;
					current = (prevbest ? prevbest : g_new (GdkRenConfigAttr, 1));
				}
			}
		}
		g_free (current);
		XFree (fbconfigs);

		if (best == NULL)
		{
			g_set_error_literal (error, GDK_REN_ERROR,
				GDK_REN_ERROR_NO_CONFIG_MATCH, NULL);
			return NULL;
		}

		GdkRenConfigOpenglX11 *config =
			g_object_new (GDK_REN_TYPE_CONFIG_OPENGL_X11, NULL);
		GdkRenConfig *renconfig = GDK_REN_CONFIG (config);
		config->uses_fbconfig = TRUE;
		config->fbconfig = best_fbconfig;
		renconfig->attributes = best;
		renconfig->backend = backend;
		renconfig->screen = g_object_ref (screen);

		return renconfig;
	}
	else
	#endif
	{
		int num_visuals;
		XVisualInfo *visuals;
		{
		XVisualInfo template;
		template.screen = xscreen;
		visuals = XGetVisualInfo (xdisplay, VisualScreenMask, &template, &num_visuals);
		}
		if (num_visuals == 0)
		{
			g_set_error_literal (error, GDK_REN_ERROR, GDK_REN_ERROR_FAILED,
				"There are no XVisualInfos available");
			return NULL;
		}

		int i;
		XVisualInfo best_visual = {0};
		GdkRenConfigAttr *best = NULL;
		GdkRenConfigAttr *current = g_new (GdkRenConfigAttr, 1);
		for (i = 0; i < num_visuals; ++i)
		{
			XVisualInfo visual = visuals[i];
			if (!visual_to_attr (xdisplay, &visual, current))
				continue;
			if (gdk_ren_config_attr_match (current, attr))
			{
				if (best == NULL || compare (best, current))
				{
					GdkRenConfigAttr *prevbest = best;
					best = current;
					best_visual = visual;
					current = (prevbest ? prevbest : g_new (GdkRenConfigAttr, 1));
				}
			}
		}
		g_free (current);
		XFree (visuals);

		if (best == NULL)
		{
			g_set_error_literal (error, GDK_REN_ERROR,
				GDK_REN_ERROR_NO_CONFIG_MATCH, NULL);
			return NULL;
		}

		GdkRenConfigOpenglX11 *config =
			g_object_new (GDK_REN_TYPE_CONFIG_OPENGL_X11, NULL);
		GdkRenConfig *renconfig = GDK_REN_CONFIG (config);
		#ifdef GLX_VERSION_1_3
		config->uses_fbconfig = FALSE;
		#endif
		config->visual = best_visual;
		renconfig->attributes = best;
		renconfig->backend = backend;
		renconfig->screen = g_object_ref (screen);

		return renconfig;
	}
}

#ifdef GLX_VERSION_1_3
static gboolean
fbconfig_to_attr (Display *xdisplay, GLXFBConfig fbconfig, GdkRenConfigAttr *attr)
{
	int value;
	#define GET_ATTRIB_OR_FAIL(attribute, place)\
		G_STMT_START {\
		if (glXGetFBConfigAttrib (xdisplay, fbconfig, (attribute), (place)) != Success)\
		{\
			g_debug ("GdkRenConfigOpenglX11: glXGetFBConfigAttrib failed for %s", G_STRINGIFY (attribute));\
			return FALSE;\
		}\
		} G_STMT_END
	#define GET_ATTRIB_INT(attribute, field)\
		G_STMT_START {\
			GET_ATTRIB_OR_FAIL ((attribute), &value);\
			attr->field = value;\
		} G_STMT_END
	#define GET_ATTRIB_BOOL(attribute, field)\
		G_STMT_START {\
			GET_ATTRIB_OR_FAIL ((attribute), &value);\
			attr->field = (value != False) ? TRUE : FALSE;\
		} G_STMT_END

	GET_ATTRIB_OR_FAIL (GLX_CONFIG_CAVEAT, &value);
	switch (value)
	{
		case GLX_NONE:
			attr->caveat = GDK_REN_CONFIG_CAVEAT_NONE;
		break;
		case GLX_SLOW_CONFIG:
			attr->caveat = GDK_REN_CONFIG_CAVEAT_SLOW;
		break;
		case GLX_NON_CONFORMANT_CONFIG:
			attr->caveat = GDK_REN_CONFIG_CAVEAT_NON_CONFORMANT;
		break;
		default:
			g_debug ("GdkRenConfigOpenglX11: Unknown Caveat: %d", value);
			return FALSE;
		break;
	}
	GET_ATTRIB_OR_FAIL (GLX_RENDER_TYPE, &value);
	{
		attr->render_type = 0;
		if (value & GLX_RGBA_BIT)
			attr->render_type |= GDK_REN_RENDER_TYPE_RGBA;
		if (value & GLX_COLOR_INDEX_BIT)
			attr->render_type |= GDK_REN_RENDER_TYPE_INDEX;
	}
	GET_ATTRIB_OR_FAIL (GLX_DRAWABLE_TYPE, &value);
	{
		attr->drawable_type = 0;
		if (value & GLX_WINDOW_BIT)
			attr->drawable_type |= GDK_REN_DRAWABLE_TYPE_WINDOW;
		if (value & GLX_PIXMAP_BIT)
			attr->drawable_type |= GDK_REN_DRAWABLE_TYPE_PIXMAP;
		if (value & GLX_PBUFFER_BIT)
			attr->drawable_type |= GDK_REN_DRAWABLE_TYPE_PBUFFER;
	}
	GET_ATTRIB_OR_FAIL (GLX_X_VISUAL_TYPE, &value);
	switch (value)
	{
		case GLX_TRUE_COLOR:
			attr->visual_type = GDK_REN_VISUAL_TYPE_TRUE_COLOR;
		break;
		case GLX_DIRECT_COLOR:
			attr->visual_type = GDK_REN_VISUAL_TYPE_DIRECT_COLOR;
		break;
		case GLX_PSEUDO_COLOR:
			attr->visual_type = GDK_REN_VISUAL_TYPE_PSEUDO_COLOR;
		break;
		case GLX_STATIC_COLOR:
			attr->visual_type = GDK_REN_VISUAL_TYPE_STATIC_COLOR;
		break;
		case GLX_GRAY_SCALE:
			attr->visual_type = GDK_REN_VISUAL_TYPE_GRAYSCALE;
		break;
		case GLX_STATIC_GRAY:
			attr->visual_type = GDK_REN_VISUAL_TYPE_STATIC_GRAY;
		break;
		case GLX_NONE:
			g_debug ("GdkRenConfigOpenglX11: VisualType is GLX_NONE");
			return FALSE;
		break;
		default:
			g_debug ("GdkRenConfigOpenglX11: Unknown VisualType: %d", value);
			return FALSE;
		break;
	}
	GET_ATTRIB_INT (GLX_LEVEL, level);
	GET_ATTRIB_INT (GLX_RED_SIZE, red_bits);
	GET_ATTRIB_INT (GLX_GREEN_SIZE, green_bits);
	GET_ATTRIB_INT (GLX_BLUE_SIZE, blue_bits);
	GET_ATTRIB_INT (GLX_ALPHA_SIZE, alpha_bits);
	attr->alpha_red_bits = 0;
	attr->alpha_green_bits = 0;
	attr->alpha_blue_bits = 0;
	GET_ATTRIB_INT (GLX_DEPTH_SIZE, depth_bits);
	GET_ATTRIB_INT (GLX_STENCIL_SIZE, stencil_bits);
	GET_ATTRIB_INT (GLX_ACCUM_RED_SIZE, accum_red_bits);
	GET_ATTRIB_INT (GLX_ACCUM_GREEN_SIZE, accum_green_bits);
	GET_ATTRIB_INT (GLX_ACCUM_BLUE_SIZE, accum_blue_bits);
	GET_ATTRIB_INT (GLX_ACCUM_ALPHA_SIZE, accum_alpha_bits);
	attr->accum_alpha_red_bits = 0;
	attr->accum_alpha_green_bits = 0;
	attr->accum_alpha_blue_bits = 0;
	GET_ATTRIB_BOOL (GLX_DOUBLEBUFFER, double_buffer);
	GET_ATTRIB_BOOL (GLX_STEREO, stereo_buffer);
	GET_ATTRIB_INT (GLX_AUX_BUFFERS, aux_buffers);
	/* TODO: Sample buffers are only in GLX 1.4 (or with GLX_ARB_multisample) */
	attr->sample_buffers = 0;
	attr->samples = 0;
	GET_ATTRIB_OR_FAIL (GLX_TRANSPARENT_TYPE, &value);
	switch (value)
	{
		case GLX_NONE:
			attr->transparent_type = GDK_REN_TRANSPARENT_TYPE_ALPHA;
		break;
		case GLX_TRANSPARENT_RGB:
			attr->transparent_type = GDK_REN_TRANSPARENT_TYPE_RGB;
		break;
		case GLX_TRANSPARENT_INDEX:
			attr->transparent_type = GDK_REN_TRANSPARENT_TYPE_INDEX;
		break;
		default:
			g_debug ("GdkRenConfigOpenglX11: Unknown TransparentType: %d", value);
			return FALSE;
		break;
	}
	GET_ATTRIB_INT (GLX_TRANSPARENT_INDEX_VALUE, transparent_index_value);
	GET_ATTRIB_INT (GLX_TRANSPARENT_RED_VALUE, transparent_red_value);
	GET_ATTRIB_INT (GLX_TRANSPARENT_GREEN_VALUE, transparent_green_value);
	GET_ATTRIB_INT (GLX_TRANSPARENT_BLUE_VALUE, transparent_blue_value);

	#undef GET_ATTRIB_BOOL
	#undef GET_ATTRIB_INT
	#undef GET_ATTRIB_OR_FAIL

	return TRUE;
}
#endif

static gboolean
visual_to_attr (Display *xdisplay, XVisualInfo *visual, GdkRenConfigAttr *attr)
{
	int value;
#if 0
	#ifdef GLX_VERSION_1_3
	if (glXGetConfig (xdisplay, visual, GLX_FBCONFIG_ID, &value) == Success)
	{
		if (value != -1)
		{
			GLXFBConfig fbconfig = (GLXFBConfig) value;
			return fbconfig_to_attr (xdisplay, fbconfig, attr);
		}
	}
	#endif
#endif

	#define GET_ATTRIB_OR_FAIL(attribute, place)\
		G_STMT_START {\
		if (glXGetConfig (xdisplay, visual, (attribute), (place)) != Success)\
		{\
			g_debug ("GdkRenConfigOpenglX11: glXGetConfig failed for %s", G_STRINGIFY (attribute));\
			return FALSE;\
		}\
		} G_STMT_END
	#define GET_ATTRIB_INT(attribute, field)\
		G_STMT_START {\
			GET_ATTRIB_OR_FAIL ((attribute), &value);\
			attr->field = value;\
		} G_STMT_END
	#define GET_ATTRIB_BOOL(attribute, field)\
		G_STMT_START {\
			GET_ATTRIB_OR_FAIL ((attribute), &value);\
			attr->field = (value != False) ? TRUE : FALSE;\
		} G_STMT_END

	GET_ATTRIB_OR_FAIL (GLX_USE_GL, &value);
	if (value != True)
		return FALSE;

	attr->caveat = GDK_REN_CONFIG_CAVEAT_NONE;
	GET_ATTRIB_OR_FAIL (GLX_RGBA, &value);
	attr->render_type = (value == True) ? GDK_REN_RENDER_TYPE_RGBA : GDK_REN_RENDER_TYPE_INDEX;
	attr->drawable_type = GDK_REN_DRAWABLE_TYPE_WINDOW | GDK_REN_DRAWABLE_TYPE_PIXMAP;
	switch (visual->class)
	{
		case TrueColor:
			attr->visual_type = GDK_REN_VISUAL_TYPE_TRUE_COLOR;
		break;
		case DirectColor:
			attr->visual_type = GDK_REN_VISUAL_TYPE_DIRECT_COLOR;
		break;
		case PseudoColor:
			attr->visual_type = GDK_REN_VISUAL_TYPE_PSEUDO_COLOR;
		break;
		case StaticColor:
			attr->visual_type = GDK_REN_VISUAL_TYPE_STATIC_COLOR;
		break;
		case GrayScale:
			attr->visual_type = GDK_REN_VISUAL_TYPE_GRAYSCALE;
		break;
		case StaticGray:
			attr->visual_type = GDK_REN_VISUAL_TYPE_STATIC_GRAY;
		break;
		default:
			g_debug ("GdkRenConfigOpenglX11: Unknown VisualInfo class: %d", visual->class);
			return FALSE;
		break;
	}
	GET_ATTRIB_INT (GLX_LEVEL, level);
	GET_ATTRIB_INT (GLX_RED_SIZE, red_bits);
	GET_ATTRIB_INT (GLX_GREEN_SIZE, green_bits);
	GET_ATTRIB_INT (GLX_BLUE_SIZE, blue_bits);
	GET_ATTRIB_INT (GLX_ALPHA_SIZE, alpha_bits);
	attr->alpha_red_bits = 0;
	attr->alpha_green_bits = 0;
	attr->alpha_blue_bits = 0;
	GET_ATTRIB_INT (GLX_DEPTH_SIZE, depth_bits);
	GET_ATTRIB_INT (GLX_STENCIL_SIZE, stencil_bits);
	GET_ATTRIB_INT (GLX_ACCUM_RED_SIZE, accum_red_bits);
	GET_ATTRIB_INT (GLX_ACCUM_GREEN_SIZE, accum_green_bits);
	GET_ATTRIB_INT (GLX_ACCUM_BLUE_SIZE, accum_blue_bits);
	GET_ATTRIB_INT (GLX_ACCUM_ALPHA_SIZE, accum_alpha_bits);
	attr->accum_alpha_red_bits = 0;
	attr->accum_alpha_green_bits = 0;
	attr->accum_alpha_blue_bits = 0;
	GET_ATTRIB_BOOL (GLX_DOUBLEBUFFER, double_buffer);
	GET_ATTRIB_BOOL (GLX_STEREO, stereo_buffer);
	GET_ATTRIB_INT (GLX_AUX_BUFFERS, aux_buffers);
	/* TODO: Sample buffers are only in GLX 1.4 (or with GLX_ARB_multisample) */
	attr->sample_buffers = 0;
	attr->samples = 0;
	attr->transparent_type = GDK_REN_TRANSPARENT_TYPE_ALPHA;
	attr->transparent_index_value = 0;
	attr->transparent_red_value = 0;
	attr->transparent_green_value = 0;
	attr->transparent_blue_value = 0;

	#undef GET_ATTRIB_BOOL
	#undef GET_ATTRIB_INT
	#undef GET_ATTRIB_OR_FAIL

	return TRUE;
}

#if 0
static GArray*
attr_to_glxattr (const GdkRenConfigAttr *attr)
{
	GArray *attrib_list = g_array_sized_new (FALSE, FALSE, sizeof (int), 32);
	#define ATTR_ADD(__name, __value) g_array_append_vals (attrib_list, \
		(int[2]) {(__name), (__value)}, 2)
	if (attr->caveat == GDK_REN_CONFIG_CAVEAT_NONE)
		ATTR_ADD (GLX_CONFIG_CAVEAT, GLX_NONE);
		/* Else we have to go through the results below... GLX does not support
		bitmask for this configuration. */
	{ /* render_type */
		GdkRenRenderType render_type = attr->render_type;
		if (render_type == GDK_REN_RENDER_TYPE_ANY)
			ATTR_ADD (GLX_RENDER_TYPE, GLX_DONT_CARE);
		else
		{
			int value = 0;
			if (render_type == GDK_REN_RENDER_TYPE_RAGABA)
				goto FAIL;
			if (render_type & GDK_REN_RENDER_TYPE_RGBA)
				value |= GLX_RGBA_BIT;
			if (render_type & GDK_REN_RENDER_TYPE_INDEX)
				value |= GLX_COLOR_INDEX_BIT;
			ATTR_ADD (GLX_RENDER_TYPE, value);
		}
	}
	{ /* drawable_type */
		GdkRenDrawableType drawable_type = attr->drawable_type;
		if (drawable_type == GDK_REN_DRAWABLE_TYPE_ANY)
			ATTR_ADD (GLX_DRAWABLE_TYPE, GLX_DONT_CARE);
		else
		{
			int value = 0;
			if (drawable_type & GDK_REN_DRAWABLE_TYPE_WINDOW)
				value |= GLX_WINDOW_BIT;
			if (drawable_type & GDK_REN_DRAWABLE_TYPE_PIXMAP)
				value |= GLX_PIXMAP_BIT;
			if (drawable_type & GDK_REN_DRAWABLE_TYPE_PBUFFER)
				value |= GLX_PBUFFER_BIT;
			ATTR_ADD (GLX_DRAWABLE_TYPE, value);
		}
	}
	/* TODO */
	{ /* double and stereo buffers */
		gint double_buffer = attr->double_buffer;
		gint stereo_buffer = attr->stereo_buffer;
		if (double_buffer != -1)
			ATTR_ADD (GLX_DOUBLEBUFFER, double_buffer);
		if (stereo_buffer != -1)
			ATTR_ADD (GLX_STEREO, stereo_buffer);
	}
	/* TODO */
	ATTR_ADD (None, 0);
	#undef ATTR_ADD
	return attrib_list;
	/* Remember that caller should free this! */
	FAIL:
	if (attrib_list) g_array_free (attrib_list, TRUE);
	return NULL;
}
#endif
