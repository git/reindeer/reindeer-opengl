/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2008, 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "matrix.h"

RenMatrixBackDataKey *matrix_bd_key;

ren_size
REN_IMPL_PRIV_MANGLE(matrix_bd_size) = sizeof (RenMatrixBackData);

void
REN_IMPL_PRIV_MANGLE(matrix_bd_init) (RenMatrix *matrix,
	RenMatrixBackData *back_data, void *user_data,
	const RenMatrixInfo *info)
{
	back_data->m = info->data;
	ren_bool transposed = info->transposed;
	switch (info->type)
	{
		case REN_TYPE_SFLOAT:
			if (transposed)
			{
				back_data->float_set = glLoadTransposeMatrixf;
				back_data->float_mul = glMultTransposeMatrixf;
			}
			else
			{
				back_data->float_set = glLoadMatrixf;
				back_data->float_mul = glMultMatrixf;
			}
		return;
		case REN_TYPE_DFLOAT:
			if (transposed)
			{
				back_data->double_set = glLoadTransposeMatrixd;
				back_data->double_mul = glMultTransposeMatrixd;
			}
			else
			{
				back_data->double_set = glLoadMatrixd;
				back_data->double_mul = glMultMatrixd;
			}
		return;
		default:
		return;
	}
}
