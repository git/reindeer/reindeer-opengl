/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "opengl.h"
#include "color.h"

static inline void material_bind (RenReindeer *r,
	GLenum face, RenMaterial *material);

void
REN_IMPL_MANGLE(material_bind) (RenReindeer *r,
	RenMaterial *front, RenMaterial *back)
{
	if (front == back)
	{
		material_bind (r, GL_FRONT_AND_BACK, front);
	}
	else
	{
		material_bind (r, GL_FRONT, front);
		material_bind (r, GL_BACK, back);
	}
}

void
REN_IMPL_MANGLE(material_front) (RenReindeer *r, RenMaterial *material)
{
	material_bind (r, GL_FRONT, material);
}

void
REN_IMPL_MANGLE(material_back) (RenReindeer *r, RenMaterial *material)
{
	material_bind (r, GL_BACK, material);
}

static inline void material_bind (RenReindeer *r,
	GLenum face, RenMaterial *material)
{
	if (material == NULL)
	{
		static const GLfloat black[4] = {0.0, 0.0, 0.0, 1.0};
		static const GLfloat weak[4] = {0.2, 0.2, 0.2, 1.0};
		static const GLfloat strong[4] = {0.8, 0.8, 0.8, 1.0};
		glMaterialfv (face, GL_AMBIENT, weak);
		glMaterialfv (face, GL_DIFFUSE, strong);
		glMaterialfv (face, GL_SPECULAR, black);
		glMaterialfv (face, GL_EMISSION, black);
		glMaterialf (face, GL_SHININESS, 0.0);
		return;
	}

	const RenMaterialInfo *info = ren_material_info (material);
	if (info->ambient == info->diffuse)
	{
		glMaterialfv (face, GL_AMBIENT_AND_DIFFUSE, get_color (info->ambient, r));
	}
	else
	{
		glMaterialfv (face, GL_AMBIENT, get_color (info->ambient, r));
		glMaterialfv (face, GL_DIFFUSE, get_color (info->diffuse, r));
	}
	glMaterialfv (face, GL_SPECULAR, get_color (info->specular, r));
	glMaterialfv (face, GL_EMISSION, get_color (info->emission, r));
	glMaterialf (face, GL_SHININESS, (GLfloat) info->shininess);
}
