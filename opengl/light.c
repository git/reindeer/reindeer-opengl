/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2008, 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "opengl.h"
#include "color.h"

static inline void
light_bind (RenReindeer *r, RenLight *light, ren_size index);

void
REN_IMPL_MANGLE(lighting_enable) (RenReindeer *r)
{
	RenReindeerBackData *r_bd = ren_reindeer_back_data (r);
	r_bd->lighting_enabled = TRUE;
	glEnable (GL_LIGHTING);
}

void
REN_IMPL_MANGLE(lighting_disable) (RenReindeer *r)
{
	RenReindeerBackData *r_bd = ren_reindeer_back_data (r);
	r_bd->lighting_enabled = FALSE;
	glDisable (GL_LIGHTING);
}

void
REN_IMPL_MANGLE(lighting_ambient) (RenReindeer *r, RenColor *color)
{
	glLightModelfv (GL_LIGHT_MODEL_AMBIENT, get_color (color, r));
}

/* TODO: We could probably make much faster implementations of the functions
below if we had back data for lights. Also, perhaps use a different data
structure to keep track of enabled lights.  */

ren_size
REN_IMPL_MANGLE(light_bind) (RenReindeer *r, ren_size n, RenLight **lights)
{
	if (n == 0 || lights == NULL)
		return 0;

	RenReindeerBackData *r_bd = ren_reindeer_back_data (r);
	ren_size max_lights = r_bd->max_lights;
	ren_size i, j;

	n = MIN (n, max_lights);
	ren_size is_here[n];
	for (j = 0; j < n; ++j)
		is_here[j] = max_lights;
	ren_size here_is[max_lights];
	for (i = 0; i < max_lights; ++i)
		here_is[i] = n;

	for (i = 0; i < max_lights; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			if (r_bd->lights[i] == lights[j])
			{
				is_here[j] = i;
				here_is[i] = j;
				goto NEXT_I;
			}
		}
		if (r_bd->light_is_enabled[i])
		{
			glDisable (GL_LIGHT0 + i);
			r_bd->light_is_enabled[i] = FALSE;
		}
		NEXT_I: ;
	}

	for (j = 0; j < n; ++j)
	{
		i = is_here[j];
		if (i == max_lights)
		{
			for (i = 0; i < max_lights; ++i)
			{
				if (here_is[i] == n && !r_bd->light_is_enabled[i])
				{
					light_bind (r, lights[j], i);
					glEnable (GL_LIGHT0 + i);
					r_bd->lights[i] = lights[j];
					r_bd->light_is_enabled[i] = TRUE;
					goto NEXT_J;
				}
			}
		}
		else
		{
			if (!r_bd->light_is_enabled[i])
			{
				glEnable (GL_LIGHT0 + i);
				r_bd->light_is_enabled[i] = TRUE;
			}
		}
		NEXT_J: ;
	}

	return n;
}

ren_bool
REN_IMPL_MANGLE(light_enable) (RenReindeer *r, RenLight *light)
{
	if (light == NULL)
		return TRUE;

	RenReindeerBackData *r_bd = ren_reindeer_back_data (r);
	ren_size i;
	ren_size first_free = r_bd->max_lights;

	for (i = 0; i < r_bd->max_lights; ++i)
	{
		if (r_bd->lights[i] == light)
		{
			if (!r_bd->light_is_enabled[i])
			{
				glEnable (GL_LIGHT0 + i);
				r_bd->light_is_enabled[i] = TRUE;
			}
			return TRUE;
		}
		else if (first_free == r_bd->max_lights && !r_bd->light_is_enabled[i])
		{
			first_free = i;
		}
	}
	if (first_free != r_bd->max_lights)
	{
		light_bind (r, light, first_free);
		glEnable (GL_LIGHT0 + first_free);
		r_bd->lights[first_free] = light;
		r_bd->light_is_enabled[first_free] = TRUE;
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

void
REN_IMPL_MANGLE(light_disable) (RenReindeer *r, RenLight *light)
{
	RenReindeerBackData *r_bd = ren_reindeer_back_data (r);
	ren_size i;
	for (i = 0; i < r_bd->max_lights; ++i)
	{
		if (r_bd->lights[i] == light)
		{
			if (r_bd->light_is_enabled[i])
			{
				glDisable (GL_LIGHT0 + i);
				r_bd->light_is_enabled[i] = FALSE;
			}
			return;
		}
	}
}

#if 0
static inline const GLfloat *
apply_attenuation (GLenum light_id, const RenVector *attenuation)
{
	GLfloat *k = ren_vector_backend_data (attenuation, r)->float_data;
	glLightf (light_id, GL_CONSTANT_ATTENUATION, k[0]);
	glLightf (light_id, GL_LINEAR_ATTENUATION, k[1]);
	glLightf (light_id, GL_QUADRATIC_ATTENUATION, k[2]);
}
#endif

static inline void
light_bind (RenReindeer *r, RenLight *light, ren_size index)
{
	GLenum light_id = GL_LIGHT0 + index;

	const RenLightInfo *info = ren_light_info (light);

	glLightfv (light_id, GL_AMBIENT, get_color (info->ambient, r));
	glLightfv (light_id, GL_DIFFUSE, get_color (info->diffuse, r));
	glLightfv (light_id, GL_SPECULAR, get_color (info->specular, r));

	switch (info->light_type)
	{
		case REN_LIGHT_TYPE_POINT_LIGHT:
		{
			#if 0
			const RenVector *attenuation;
			_ren_light_data_point_light (light, &attenuation);
			apply_attenuation (light_id, attenuation);
			#endif
			glLightf (light_id, GL_SPOT_CUTOFF, 180.0);
			glLightfv (light_id, GL_POSITION,
				(GLfloat[4]) {0.0, 0.0, 0.0, 1.0});
		}
		break;
		case REN_LIGHT_TYPE_DIRECTIONAL:
		{
			glLightfv (light_id, GL_POSITION,
				(GLfloat[4]) {0.0, 0.0, 1.0, 0.0});
		}
		break;
		case REN_LIGHT_TYPE_SPOT_LIGHT:
		{
			glLightf (light_id, GL_SPOT_EXPONENT, info->exponent);
			glLightf (light_id, GL_SPOT_CUTOFF, info->cutoff);
			glLightfv (light_id, GL_POSITION,
				(GLfloat[4]) {0.0, 0.0, 0.0, 1.0});
			glLightfv (light_id, GL_SPOT_DIRECTION,
				(GLfloat[3]) {0.0, 0.0, 1.0});
		}
		break;
		default:
		break;
	}
}
