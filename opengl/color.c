/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2008, 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "color.h"

RenColorBackDataKey *color_bd_key;

ren_size
REN_IMPL_PRIV_MANGLE(color_bd_size) = sizeof (RenColorBackData);

#if 0
void
REN_IMPL_PRIV_MANGLE(color_bd_init) (RenColor *color,
	RenColorBackData *back_data, void *user_data)
{

}
#endif

#if 0
void
REN_IMPL_PRIV_MANGLE(color_bd_fini) (RenColor *color,
	RenColorBackData *back_data, void *user_data)
{
	if (back_data->converted != NULL)
	{
		_ren_color_convert_unref (color,
			REN_COLOR_FORMAT_RGBA, REN_TYPE_SFLOAT);
	}
}
#endif

void
REN_IMPL_PRIV_MANGLE(color_bd_update) (RenColor *color,
	RenColorBackData *back_data, void *user_data,
	const RenColorInfo *info)
{
	#if 0 /* TODO */
	if (info->format != REN_COLOR_FORMAT_RGBA || info->type != REN_TYPE_SFLOAT)
	{
		back_data->converted = _ren_color_convert_ref (color,
			REN_COLOR_FORMAT_RGBA, REN_TYPE_SFLOAT);
		back_data->color_data = back_data->converted;
	}
	else
	#endif
	{
		back_data->converted = NULL;
		back_data->color_data = info->data;
	}
}
