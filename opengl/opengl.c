/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2008, 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "opengl.h"
#include "color.h"
#include "matrix.h"

#include <stdlib.h>

const char* REN_IMPL_MANGLE(compat_version) = "0.2";

ren_bool
REN_IMPL_MANGLE(backend_init) (RenBackend *backend)
{
	color_bd_key = ren_color_back_data_key_new (
		REN_IMPL_PRIV_MANGLE(color_bd_size),
		NULL,
		NULL,
		REN_IMPL_PRIV_MANGLE(color_bd_update)
	);
	matrix_bd_key = ren_matrix_back_data_key_new (
		REN_IMPL_PRIV_MANGLE(matrix_bd_size),
		REN_IMPL_PRIV_MANGLE(matrix_bd_init),
		NULL,
		NULL
	);
	return TRUE;
}

ren_bool
REN_IMPL_MANGLE(backend_fini) (void)
{
	ren_color_back_data_key_unref (color_bd_key);
	ren_matrix_back_data_key_unref (matrix_bd_key);
	return TRUE;
}

ren_size REN_IMPL_MANGLE(reindeer_back_data_size) = sizeof (RenReindeerBackData);

void
REN_IMPL_MANGLE(reindeer_init) (RenReindeer *r, RenReindeerBackData *r_bd)
{
}

void
REN_IMPL_MANGLE(reindeer_fini) (RenReindeer *r, RenReindeerBackData *r_bd)
{
	free (r_bd->light_is_enabled);
	free (r_bd->lights);
}

ren_bool
REN_IMPL_MANGLE(init) (RenReindeer *r)
{
	RenReindeerBackData *r_bd = ren_reindeer_back_data (r);

	glFrontFace (GL_CW);
	glEnable (GL_CULL_FACE);
	glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glLightModeli (GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);

	GLint max_lights;
	glGetIntegerv (GL_MAX_LIGHTS, &max_lights);
	r_bd->max_lights = max_lights;
	r_bd->light_is_enabled = calloc (max_lights, sizeof (ren_bool));
	r_bd->lights = calloc (max_lights, sizeof (RenLight*));

	return TRUE;
}

void
REN_IMPL_MANGLE(flush) (RenReindeer *r)
{
	glFlush ();
}

void
REN_IMPL_MANGLE(finish) (RenReindeer *r)
{
	glFinish ();
}
