/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2008, 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "opengl.h"
#include "conversion.h"
#include "color.h"

void
REN_IMPL_MANGLE(object_render) (RenReindeer *r, RenObject *object)
{
	RenReindeerBackData *r_bd = ren_reindeer_back_data (r);
	const RenObjectInfo *object_info = ren_object_info (object);
	RenTemplate *tmplt = object_info->tmplt;
	const RenTemplateInfo *tmplt_info = ren_template_info (tmplt);

	if (tmplt_info->index_array != NULL)
		REN_IMPL_MANGLE(index_array_bind) (r, tmplt_info->index_array);
	REN_IMPL_MANGLE(coord_array_bind) (r, object_info->coord_array);
	if (object_info->color_array != NULL)
		REN_IMPL_MANGLE(color_array_bind) (r, object_info->color_array);
	if (object_info->normal_array != NULL)
		REN_IMPL_MANGLE(normal_array_bind) (r, object_info->normal_array);

	ren_size num_primitives = tmplt_info->num_primitives;
	const RenTemplatePrimitive *primitives = tmplt_info->primitives;
	const RenTemplateMode *modes = tmplt_info->modes;

	/* Draw all primitives.  */
	const RenTemplatePrimitive *primitive;
	ren_uint32 mode;
	ren_uint32 prev_mode;
	ren_size i;
	for (i = 0; i < num_primitives; ++i)
	{
		primitive = &primitives[i];
		mode = primitive->mode;
		if (i == 0 || prev_mode != mode)
		{
			if (i == 0)
			{
				RenState state =
					object_info->state |
					modes[mode].state;
				if (r_bd->lighting_enabled)
					state |= REN_STATE_LIGHTS;
				REN_IMPL_MANGLE(state_keep) (r, state);
			}

			ren_bool done = FALSE;
			const void *next = modes[mode].data;
			const void *prev = (i > 0) ? modes[prev_mode].data : NULL;
			RenTemplateModeCmd mc;
			const RenTemplateModeData *md;
			do
			{
				ren_template_mode_switch (&next, &prev,
					&mc, &md);
				switch (mc)
				{
					case REN_TEMPLATE_MODE_CMD_END:
						done = TRUE;
						break;
					case REN_TEMPLATE_MODE_CMD_MATERIAL:
					{
	if (md == NULL)
	{
		REN_IMPL_MANGLE(material_bind) (r, NULL, NULL);
	}
	else
	{
		RenMaterial *front = (md->material.front >= 0) ?
			object_info->materials[md->material.front] : NULL;
		RenMaterial *back = (md->material.back >= 0) ?
			object_info->materials[md->material.back] : NULL;
		REN_IMPL_MANGLE(material_bind) (r, front, back);
	}
					}	break;
					default:
						break;
				}
			} while (!done);
			prev_mode = mode;
		}
		REN_IMPL_MANGLE(primitive) (r, primitive->prim,
			primitive->offset, primitive->count);
	}
}
