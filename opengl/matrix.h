/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _REN__OPENGL__MATRIX_H
#define _REN__OPENGL__MATRIX_H

#include "opengl.h"

struct _RenMatrixBackData
{
	union
	{
		void (* set) (const void *m);
		void (* float_set) (const GLfloat *m);
		void (* double_set) (const GLdouble *m);
	};
	union
	{
		void (* mul) (const void *m);
		void (* float_mul) (const GLfloat *m);
		void (* double_mul) (const GLdouble *m);
	};
	union
	{
		const void *m;
		const GLfloat *float_m;
		const GLdouble *double_m;
	};
};

#define matrix_bd_key REN_IMPL_PRIV_MANGLE (matrix_bd_key)
extern RenMatrixBackDataKey *matrix_bd_key;

extern ren_size REN_IMPL_PRIV_MANGLE(matrix_bd_size);

extern void
REN_IMPL_PRIV_MANGLE(matrix_bd_init) (RenMatrix *matrix,
	RenMatrixBackData *back_data, void *user_data,
	const RenMatrixInfo *info);

#endif /* _REN__OPENGL__MATRIX_H */
