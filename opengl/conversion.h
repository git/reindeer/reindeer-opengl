/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2008, 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _REN__OPENGL__CONVERSION_H
#define _REN__OPENGL__CONVERSION_H

static inline GLenum
convert_ren_type_to_gl_type (RenType ren_type)
{
	/* FIXME: For now these are hardcoded because they at least work on most
	GNU/Linux based systems using Mesa. Later, autoconf magic will need to
	define the correct gl types that each ren type suitably maps to.  */
	/* NOTE: -1 means no suitable type so conversion on the actual values are
	needed instead.  */
	static const GLenum gl_types[] =
	{
		-1, /*REN_TYPE_NONE*/
		GL_UNSIGNED_BYTE, /*REN_TYPE_BOOL*/
		GL_BYTE, /*REN_TYPE_SINT08*/
		GL_UNSIGNED_BYTE, /*REN_TYPE_UINT08*/
		GL_SHORT, /*REN_TYPE_SINT16*/
		GL_UNSIGNED_SHORT, /*REN_TYPE_UINT16*/
		GL_INT, /*REN_TYPE_SINT32*/
		GL_UNSIGNED_INT, /*REN_TYPE_UINT32*/
		-1, /*REN_TYPE_SINT64*/
		-1, /*REN_TYPE_UINT64*/
		GL_FLOAT, /*REN_TYPE_SFLOAT*/
		GL_DOUBLE, /*REN_TYPE_DFLOAT*/
	};
	if (ren_type < (sizeof (gl_types)/sizeof (gl_types[0])))
		return gl_types[ren_type];
	else
		return -2;
}

static inline GLenum
convert_ren_prim_to_gl_prim (RenPrimitive ren_prim)
{
	static const GLenum gl_prims[] =
	{
		GL_POINTS, /*REN_PRIMITIVE_POINTS*/
		GL_LINES, /*REN_PRIMITIVE_LINES*/
		GL_LINE_STRIP, /*REN_PRIMITIVE_LINE_STRIP*/
		GL_LINE_LOOP, /*REN_PRIMITIVE_LINE_LOOP*/
		GL_TRIANGLES, /*REN_PRIMITIVE_TRIANGLES*/
		GL_TRIANGLE_FAN, /*REN_PRIMITIVE_TRIANGLE_FAN*/
		GL_TRIANGLE_STRIP, /*REN_PRIMITIVE_TRIANGLE_STRIP*/
		GL_QUADS, /*REN_PRIMITIVE_QUADS*/
		GL_QUAD_STRIP, /*REN_PRIMITIVE_QUAD_STRIP*/
		-1, /*REN_PRIMITIVE_CIRCLES*/
		-1, /*REN_PRIMITIVE_ELLIPSES*/
		GL_POLYGON, /*REN_PRIMITIVE_POLYGON*/
		-1, /*REN_PRIMITIVE_SPHERES*/
		-1, /*REN_PRIMITIVE_CYLINDERS*/
	};
	if (ren_prim < (sizeof (gl_prims)/sizeof (gl_prims[0])))
		return gl_prims[ren_prim];
	else
		return -2;
}

static inline GLenum
convert_ren_test_to_gl_test (RenTest ren_test)
{
	static const GLenum gl_tests[] =
	{
		-1, /*REN_TEST_NONE*/
		GL_ALWAYS, /*REN_TEST_ALWAYS*/
		GL_NEVER, /*REN_TEST_NEVER*/
		GL_EQUAL, /*REN_TEST_EQUAL*/
		GL_NOTEQUAL, /*REN_TEST_NOT_EQUAL*/
		GL_LESS, /*REN_TEST_LESS*/
		GL_LEQUAL, /*REN_TEST_LESS_EQUAL*/
		GL_GREATER, /*REN_TEST_GREATER*/
		GL_GEQUAL, /*REN_TEST_GREATER_EQUAL*/
	};
	if (ren_test < (sizeof (gl_tests)/sizeof (gl_tests[0])))
		return gl_tests[ren_test];
	else
		return -2;
}

static inline ren_size
opengl_type_sizeof (GLenum type)
{
	switch (type)
	{
		case GL_BYTE: return sizeof (GLbyte);
		case GL_UNSIGNED_BYTE: return sizeof (GLubyte);
		case GL_SHORT: return sizeof (GLshort);
		case GL_UNSIGNED_SHORT: return sizeof (GLushort);
		case GL_INT: return sizeof (GLint);
		case GL_UNSIGNED_INT: return sizeof (GLuint);
		case GL_FLOAT: return sizeof (GLfloat);
		case GL_DOUBLE: return sizeof (GLdouble);
	}
	return 0;
}

#endif /* _REN__OPENGL__CONVERSION_H */
