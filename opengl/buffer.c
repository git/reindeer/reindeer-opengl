/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2008, 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "opengl.h"
#include "conversion.h"
#include "color.h"

void
REN_IMPL_MANGLE(clear) (RenReindeer *r, RenBuffer buffers)
{
	GLbitfield arg = 0;

	if (buffers & REN_BUFFER_COLOR)   arg |= GL_COLOR_BUFFER_BIT;
	if (buffers & REN_BUFFER_DEPTH)   arg |= GL_DEPTH_BUFFER_BIT;
	if (buffers & REN_BUFFER_STENCIL) arg |= GL_STENCIL_BUFFER_BIT;
	if (buffers & REN_BUFFER_ACCUM)   arg |= GL_ACCUM_BUFFER_BIT;

	glClear (arg);
}

void
REN_IMPL_MANGLE(clear_color) (RenReindeer *r, RenColor *color)
{
	const GLfloat *c = get_color (color, r);
	glClearColor (c[0], c[1], c[2], c[3]);
}

void
REN_IMPL_MANGLE(clear_depth) (RenReindeer *r, ren_dfloat depth)
{
	glClearDepth (depth);
}

void
REN_IMPL_MANGLE(depth_test) (RenReindeer *r, RenTest test)
{
	if (test == REN_TEST_NONE)
		glDisable (GL_DEPTH_TEST);
	else
	{
		glEnable (GL_DEPTH_TEST);
		glDepthFunc (convert_ren_test_to_gl_test (test));
	}
}
