/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2008, 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _REN__OPENGL__COLOR_H
#define _REN__OPENGL__COLOR_H

#include "opengl.h"

struct _RenColorBackData
{
	const GLfloat *color_data;
	GLfloat *converted;
};

#define color_bd_key REN_IMPL_PRIV_MANGLE (color_bd_key)
extern RenColorBackDataKey *color_bd_key;

extern ren_size REN_IMPL_PRIV_MANGLE(color_bd_size);

extern void
REN_IMPL_PRIV_MANGLE(color_bd_update) (RenColor *color,
	RenColorBackData *back_data, void *user_data,
	const RenColorInfo *info);

static inline const GLfloat*
get_color (RenColor *color, RenReindeer *r)
{
	static const GLfloat black[4] = {0.0, 0.0, 0.0, 1.0};
	if (color == NULL)
		return black;
	else
		return ren_color_back_data (color, color_bd_key)->color_data;
}

#endif /* _REN__OPENGL__COLOR_H */
