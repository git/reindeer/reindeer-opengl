/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2008, 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _REN__OPENGL__OPENGL_H
#define _REN__OPENGL__OPENGL_H

#ifndef REN_IMPL_NAME
#define REN_IMPL_NAME opengl
#endif

#include <ren/ren.h>
#include <ren/impl.h>

#include <GL/gl.h>

struct _RenReindeerBackData
{
	RenVectorArray* curr_coord_array;
	RenVectorArray* prev_coord_array;
	RenColorArray* curr_color_array;
	RenColorArray* prev_color_array;
	RenVectorArray* curr_normal_array;
	RenVectorArray* prev_normal_array;

	const GLvoid *ixarrayp; /* NULL if server element array is bound. */
	GLenum ixarray_type;

	ren_bool lighting_enabled;
	ren_size max_lights;
	ren_bool *light_is_enabled;
	RenLight **lights;
};

#define _REN_FUNC(F)\
	_REN_RET(F) REN_IMPL_MANGLE(F) _REN_PRM(F);
#include <ren/funcs.h>
#undef _REN_FUNC

#endif /* _REN__OPENGL__OPENGL_H */
