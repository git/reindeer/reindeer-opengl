/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2008, 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "opengl.h"
#include "conversion.h"

ren_bool
REN_IMPL_MANGLE(index_array_bind) (RenReindeer *r, RenIndexArray *ixarray)
{
	RenReindeerBackData *back_data = ren_reindeer_back_data (r);

	if (ixarray != NULL)
	{
		RenType type;
		RenDataBlock *datablock;
		ren_size start;
		ren_size count;
		ren_index_array_data (ixarray, &type, &datablock, &start, &count);

		const void *data = ren_data_block_info (datablock)->data;

		back_data->ixarrayp = data;
		back_data->ixarray_type = convert_ren_type_to_gl_type (type);
	}
	else
	{
		back_data->ixarrayp = NULL;
	}

	return TRUE;
}
