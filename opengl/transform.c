/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "opengl.h"
#include "matrix.h"

void
REN_IMPL_MANGLE(transform_mode) (RenReindeer *r, RenTransformMode mode)
{
	switch (mode)
	{
		case REN_TRANSFORM_MODE_MODELVIEW:
			return glMatrixMode (GL_MODELVIEW);
		case REN_TRANSFORM_MODE_PROJECTION:
			return glMatrixMode (GL_PROJECTION);
		case REN_TRANSFORM_MODE_COLOR:
			return glMatrixMode (GL_COLOR);
		case REN_TRANSFORM_MODE_TEXTURE:
			return glMatrixMode (GL_TEXTURE);
	}
}

void
REN_IMPL_MANGLE(transform_set) (RenReindeer *r, RenMatrix *matrix)
{
	RenMatrixBackData *back_data = ren_matrix_back_data (matrix, matrix_bd_key);
	back_data->set (back_data->m);
}

void
REN_IMPL_MANGLE(transform_mul) (RenReindeer *r, RenMatrix *matrix)
{
	RenMatrixBackData *back_data = ren_matrix_back_data (matrix, matrix_bd_key);
	back_data->mul (back_data->m);
}

void
REN_IMPL_MANGLE(transform_identity) (RenReindeer *r)
{
	glLoadIdentity ();
}

void
REN_IMPL_MANGLE(transform_push) (RenReindeer *r)
{
	glPushMatrix ();
}

void
REN_IMPL_MANGLE(transform_pop) (RenReindeer *r)
{
	glPopMatrix ();
}

void
REN_IMPL_MANGLE(viewport) (RenReindeer *r,
	ren_uint32 x, ren_uint32 y,
	ren_uint32 width, ren_uint32 height)
{
	glViewport (x, y, width, height);
}
