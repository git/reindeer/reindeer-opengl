/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2008, 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "opengl.h"
#include "conversion.h"

static inline ren_bool
normal_array_bind (RenReindeer *r, RenVectorArray *array);

ren_bool
REN_IMPL_MANGLE(normal_array_bind) (RenReindeer *r, RenVectorArray *array)
{
	RenReindeerBackData *r_bd = ren_reindeer_back_data (r);
	if (r_bd->curr_normal_array != array)
	{
		if (array == NULL)
		{
			r_bd->prev_normal_array = r_bd->curr_normal_array;
			glDisableClientState (GL_NORMAL_ARRAY);
		}
		else
		{
			if (r_bd->curr_normal_array != NULL ||
				array != r_bd->prev_normal_array)
			{
				if (!normal_array_bind (r, array))
					return FALSE;
				if (r_bd->curr_normal_array == NULL)
					glEnableClientState (GL_NORMAL_ARRAY);
			}
			else
			{
				glEnableClientState (GL_NORMAL_ARRAY);
			}
		}
		r_bd->curr_normal_array = array;
	}
	return TRUE;
}

static inline ren_bool
normal_array_bind (RenReindeer *r, RenVectorArray *array)
{
	RenType type;
	ren_uint08 num;
	ren_vector_array_type (array, &type, &num);
	GLenum gl_type = convert_ren_type_to_gl_type (type);
	if (gl_type == -1)
		return FALSE;

	RenDataBlock *datablock;
	ren_size start;
	ren_size count;
	ren_size stride;
	ren_vector_array_data (array, &datablock, &start, &count, &stride);

	const void *data = ren_data_block_info (datablock)->data;

	if (num > 4)
		num = 4;
	glNormalPointer (gl_type, stride, data + start);
	return TRUE;
}
