/*
	This file is part of Reindeer-OpenGL.

	Copyright (C) 2008, 2009, 2010 - Patrik Olsson

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "opengl.h"
#include "conversion.h"

void
REN_IMPL_MANGLE(state_keep) (RenReindeer *r, RenState state)
{
	RenReindeerBackData *r_bd = ren_reindeer_back_data (r);
	RenState disable = ~state;
	if (disable & REN_STATE_INDEX_ARRAY && r_bd->ixarrayp != NULL)
		REN_IMPL_MANGLE(index_array_bind) (r, NULL);
	if (disable & REN_STATE_COORD_ARRAY && r_bd->curr_coord_array != NULL)
		REN_IMPL_MANGLE(coord_array_bind) (r, NULL);
	if (disable & REN_STATE_COLOR_ARRAY && r_bd->curr_color_array != NULL)
		REN_IMPL_MANGLE(color_array_bind) (r, NULL);
	if (disable & REN_STATE_NORMAL_ARRAY && r_bd->curr_normal_array != NULL)
		REN_IMPL_MANGLE(normal_array_bind) (r, NULL);
	if (disable & REN_STATE_LIGHTS)
		REN_IMPL_MANGLE(lighting_disable) (r);
	else
		REN_IMPL_MANGLE(lighting_enable) (r);
	if (disable & REN_STATE_MATERIALS)
		REN_IMPL_MANGLE(material_bind) (r, NULL, NULL);
}

void
REN_IMPL_MANGLE(primitive) (RenReindeer *r,
	RenPrimitive prim, ren_size offset, ren_size count)
{
	RenReindeerBackData *back_data = ren_reindeer_back_data (r);
	const GLvoid *start = back_data->ixarrayp;
	ren_size vxcount = ren_primitive_vertex_count (prim, count);
	GLenum glprim = convert_ren_prim_to_gl_prim (prim);
	if (glprim == -1)
		return;
	if (start != NULL)
	{
		GLenum type = back_data->ixarray_type;
		ren_size elemsize = opengl_type_sizeof (type);
		glDrawElements (glprim, vxcount, type, start + (offset * elemsize));
	}
	else
		glDrawArrays (glprim, offset, vxcount);
}
